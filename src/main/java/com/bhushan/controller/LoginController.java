package com.bhushan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.model.Response;
import com.bhushan.service.LoginService;

@RestController
@CrossOrigin
public class LoginController {

	@Autowired
	private LoginService loginService;

	@GetMapping("fetchlogin/{user_name}/{password}")
	public Response validateLogin(@PathVariable String user_name, @PathVariable String password) throws Exception {
		Response login = loginService.login(user_name, password);
		return login;

	}

}
