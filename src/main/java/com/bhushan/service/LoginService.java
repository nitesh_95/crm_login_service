package com.bhushan.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bhushan.entity.User;
import com.bhushan.model.Response;
import com.bhushan.repo.CRMRepo;

@Service
public class LoginService {

	@Autowired
	private CRMRepo userRepo;

	public Response login(String username, String password) throws Exception {
		Response response = new Response();
		Optional<User> byuser_nameAndPassword = userRepo.getByuser_nameAndPassword(username);
		User user = byuser_nameAndPassword.get();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(password);
		boolean matches = passwordEncoder.matches(password, user.getPassword());
		if (user.getPassword().equals(password)) {
			response.setStatus(201);
			response.setMessage("Login Succeeded");
		} else {
			response.setStatus(403);
			response.setMessage("You are not Authorized");
		}

		return response;
	}

}