package com.bhushan.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bhushan.entity.User;

public interface CRMRepo extends JpaRepository<User, Integer> {

	@Query(value = "select * from user where user_name=?1  ", nativeQuery = true)
	Optional<User> getByuser_nameAndPassword(@Param("user_name") String user_name);
}
